import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
// rxjs
import { Observable } from "rxjs/Observable";
import { AuthApiService } from './../../services/auth-api/auth-api.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: []
})
export class LoginComponent implements OnInit {
  /**
   * The error if authentication fails.
   * @type {Observable<string>}
   */
  public error: Observable<string>;

  // user ip address
  ipAddress: string

  /**
   * True if the authentication is loading.
   * @type {boolean}
   */
  public loading: Observable<boolean>;

  /**
   * The authentication form.
   * @type {FormGroup}
   */
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthApiService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  appLogoSrc = "assets/images/timesheet-material-transparent.png";

  ngOnInit() {
    // clear old user session once user is navigated to this page
    this.authService.clearSession();
    // set formGroup
    this.form = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: ["", Validators.required]
    });

    this.authService.getUserIPAddress()
      .subscribe(ipData => {
        if (ipData && ipData["ip"]) {
          this.ipAddress = ipData["ip"];
        }
      })
    this.authService.getSession()
      .subscribe(
        data => {
          console.log(data)
        }
      )
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  submit() {
    // get email and password values
    const email: string = this.form.get("email").value;
    const password: string = this.form.get("password").value;

    var cred = {
      email: email.trim(), //trim value
      password: password.trim(), // trim value
      ipAddress: this.ipAddress
    }

    this.loading = Observable.of(true);

    // this.authService.authenticate(cred)
    //   .subscribe(
    //     // if everything goes well
    //     res => {
    //       if (res && res.success) {

    //         if (res.user.admin == true) {
    //           this.authService.setSession(res);
    //           setTimeout(() => {
    //             this.router.navigateByUrl('/admin');
    //             this.loading = Observable.of(false);
    //             this.error = Observable.of("")
    //           }, 2000);
    //         } else {
    //           this.authService.setSession(res);
    //           setTimeout(() => {
    //             this.router.navigateByUrl('/user');
    //             this.loading = Observable.of(false);
    //             this.error = Observable.of("")
    //           }, 2000);

    //         }
    //       }

    //       else {
    //         this.loading = Observable.of(false);
    //         let message = (res && res.message) ? res.message : "Erroring logging you in!"
    //         this.error = Observable.of(message)
    //       }

    //     },
    //     // if something goes wrong
    //     error => {
    //       this.error = Observable.of(error)
    //       this.loading = Observable.of(false);
    //     }
    //   )
  }

}
