import { Directive, ElementRef, HostListener, Input, Renderer, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[numberOnly]',
  // host: {
  //   "(input)": 'onInputChange($event)'
  // }
})

export class NumberOnlyDirective {

  regexStr = '^[0-9]*$';

  // // Use this to change model output
  // @Output() ngModelChange: EventEmitter<any> = new EventEmitter()
  // value: any

  constructor(public el: ElementRef, public renderer: Renderer) { }

  @Input() numberOnly: string;

  // // Use this to change model output
  // onInputChange($event) {
  //   this.value = $event.target.value.toUpperCase();
  //   this.ngModelChange.emit("sa");
  // }

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    let e = <KeyboardEvent>event;
    if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode == 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode == 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode == 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode == 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }
    let ch = String.fromCharCode(e.keyCode);
    let regEx = new RegExp(this.regexStr);
    if (regEx.test(ch)) {
      return;
    } else {
      e.preventDefault();
    }
  }
}