import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { UploadFileComponent } from './new-file/upload-file/upload-file.component';
import { FileSettingsComponent } from './new-file/file-settings/file-settings.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        // loadChildren: './admin-dashboard/admin-dashboard.module#AdminDashboardModule'
        component: AdminDashboardComponent,
      }, {
        path: 'new-file/upload',
        component: UploadFileComponent,
      }, {
        path: 'new-file/file-settings',
        component: FileSettingsComponent,
      }, {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
