import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { SharedService } from './../../../services/shared/shared.service';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { NumberValidatorService } from './../../../services/form-validators/number-validator.service';

@Component({
  selector: 'app-file-settings',
  templateUrl: './file-settings.component.html',
  styleUrls: ['./file-settings.component.scss']
})
export class FileSettingsComponent implements OnInit {
  userFile: any;
  public timesheetForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private dataStorage: SharedService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {
    this.userFile = this.dataStorage.storage;
  }

  ngOnInit() {
    this.dataStorage.storage = null;
    this.initializeTimesheetForm();
  }

  initializeTimesheetForm(): void {
    let fileName = (this.userFile && this.userFile['fileName']) ? this.userFile['fileName'] : '';
    let fileType = (this.userFile && this.userFile['fileType']) ? this.userFile['fileType'] : '';
    let fileSize = (this.userFile && this.userFile['fileSize']) ? this.userFile['fileSize'] : '';
    this.timesheetForm = this.formBuilder.group({
      fileName: new FormControl(fileName, [Validators.required]),
      fileType: new FormControl(fileType, [Validators.required]),
      fileSize: new FormControl(fileSize, [Validators.required])
    });
  }

  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

}
