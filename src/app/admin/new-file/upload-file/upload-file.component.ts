import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';
import { DropzoneComponent, DropzoneDirective, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from './../../../services/shared/shared.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataStorage: SharedService,
    private toastr: ToastrService
  ) {
  }

  public type: string = 'component';

  public disabled: boolean = false;

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };

  @ViewChild(DropzoneComponent) componentRef: DropzoneComponent;
  @ViewChild(DropzoneDirective) directiveRef: DropzoneDirective;

  public toggleType(): void {
    this.type = (this.type === 'component') ? 'directive' : 'component';
  }

  public toggleDisabled(): void {
    this.disabled = !this.disabled;
  }

  public toggleAutoReset(): void {
    this.config.autoReset = this.config.autoReset ? null : 5000;
    this.config.errorReset = this.config.errorReset ? null : 5000;
    this.config.cancelReset = this.config.cancelReset ? null : 5000;
  }

  public toggleMultiUpload(): void {
    this.config.maxFiles = this.config.maxFiles ? null : 1;
  }

  public toggleClickAction(): void {
    this.config.clickable = !this.config.clickable;
  }

  public resetDropzoneUploads(): void {
    if (this.type === 'directive') {
      this.directiveRef.reset();
    } else {
      this.componentRef.directiveRef.reset();
    }
  }

  public onUploadError(args: any): void {
    console.log(args)
    if (args instanceof Array && args.length > 0 && args[0]) {
      if (args[0]['status'] == 'error') {
        this.toastr.error(args[1], 'Error');
        this.resetDropzoneUploads();
      }
    }
  }

  public onUploadSuccess(args: any): void {
    console.log(args)
    if (args instanceof Array && args.length > 0 && args[0]) {
      let file = args[0];
      let navigationExtras: NavigationExtras = {
        queryParams: {
          "fileName": file.name,
          "fileType": file.type,
          "fileSize": parseInt(file.size) / 1000 + " kb"
        }
      };
      this.dataStorage.storage = {
        "fileName": file.name,
        "fileType": file.type,
        "fileSize": parseInt(file.size) / 1000 + " kb"
      }
      this.router.navigate(["/admin/new-file/file-settings"]);

    } else {
      alert("Error concurred!")
    }
  }

  ngOnInit() {

  }

}
