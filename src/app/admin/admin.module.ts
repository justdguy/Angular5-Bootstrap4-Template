import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgUploaderModule } from 'ngx-uploader';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { UploadFileComponent } from './new-file/upload-file/upload-file.component';
import { FileSettingsComponent } from './new-file/file-settings/file-settings.component';
import { DropzoneModule } from 'ngx-dropzone-wrapper';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    NgbModule,
    DropzoneModule,
    NgUploaderModule
  ],
  declarations: [AdminComponent, AdminDashboardComponent, AdminSidebarComponent, AdminHeaderComponent, AdminFooterComponent, UploadFileComponent, FileSettingsComponent]
})
export class AdminModule { }
