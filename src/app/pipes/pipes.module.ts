import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatDatePipe, FormatDateTimePipe } from './../pipes/format-date/format-date.pipe';

const pipes = [
  FormatDatePipe,
  FormatDateTimePipe
]

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [...pipes],
  exports: [...pipes]
})
export class PipesModule { }
