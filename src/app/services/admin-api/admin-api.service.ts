import { Injectable } from '@angular/core';
import { RestApiBaseService } from './../rest-api-base/rest-api-base.service';

@Injectable()
export class AdminApiService {

  constructor(
    private restApiService: RestApiBaseService
  ) { }

  getUsers(){
    return this.restApiService.get("/users");
  }

  addUser(user){
    return this.restApiService.post("/addUser", {...user});
  }

  addProject(project){
    return this.restApiService.post("/addProject", {...project});
  }

  updateProject(project){
    return this.restApiService.post("/updateProject", {...project});
  }

  deleteProject(project){
    return this.restApiService.delete("/deleteProject", {...project});
  }

  addRole(role){
    return this.restApiService.post("/addRole", {...role});
  }

  updateRole(role){
    return this.restApiService.post("/updateRole", {...role});
  }

  deleteRole(role){
    return this.restApiService.delete("/deleteRole", {...role});
  }

  getRoles(){
    return this.restApiService.get("/roles");
  }

  assignAUser(assignAUser){
    if(assignAUser){
      assignAUser["userID"] = localStorage.getItem("userID");
    }
    return this.restApiService.post("/assignUser", {...assignAUser});
  }

  updateAssignedUser(assignAUser){
    return this.restApiService.post("/updateAssignedRole", {...assignAUser});
  }

  getAssignedUsers(){
    return this.restApiService.get("/assignedUsers");
  }

}
