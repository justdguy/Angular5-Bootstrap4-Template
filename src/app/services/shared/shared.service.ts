import { Injectable } from '@angular/core';
import { RestApiBaseService } from './../rest-api-base/rest-api-base.service';

@Injectable()
export class SharedService {

  public storage: any;

  constructor(private restApiBase: RestApiBaseService) { }

  getAllProjects(){
    return this.restApiBase.get("/projects" )
  }

}
