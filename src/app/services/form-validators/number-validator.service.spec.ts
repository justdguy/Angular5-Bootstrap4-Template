import { TestBed, inject } from '@angular/core/testing';

import { NumberValidatorService } from './number-validator.service';

describe('NumberValidatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NumberValidatorService]
    });
  });

  it('should be created', inject([NumberValidatorService], (service: NumberValidatorService) => {
    expect(service).toBeTruthy();
  }));
});
