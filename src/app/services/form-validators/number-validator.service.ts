import { Injectable } from '@angular/core';
import { FormControl, ValidatorFn, Validators } from '@angular/forms';

@Injectable()
export class NumberValidatorService {

  constructor() { }

  static number(prms: any): ValidatorFn {
    return (control: FormControl): any => {
      if (Validators.required(control).isPresent()) {
        return null;
      }

      let val: number = control.value;

      if (isNaN(val) || /\D/.test(val.toString())) {

        return { "number": true };
      } else if (!isNaN(prms.min) && !isNaN(prms.max)) {

        return val < prms.min || val > prms.max ? { "number": true } : null;
      } else if (!isNaN(prms.min)) {

        return val < prms.min ? { "number": true } : null;
      } else if (!isNaN(prms.max)) {

        return val > prms.max ? { "number": true } : null;
      } else {

        return null;
      }
    };
  }

  static max(max: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {

      let val: number = control.value;

      if (control.pristine || control.pristine) {
        return null;
      }
      if (val <= max) {
        return null;
      }
      return { 'max': true };
    }
  }

  static min(min: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {

      let val: number = control.value;

      if (control.pristine || control.pristine) {
        return null;
      }
      if (val >= min) {
        return null;
      }
      return { 'min': true };
    }

  }
}