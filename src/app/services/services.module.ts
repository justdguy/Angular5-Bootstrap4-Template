import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminApiService } from './admin-api/admin-api.service';
import { AuthApiService } from './auth-api/auth-api.service';
import { RestApiBaseService } from './rest-api-base/rest-api-base.service';
import { SharedService } from './shared/shared.service';
import { StaffApiService } from './staff-api/staff-api.service';
import { NumberValidatorService } from './form-validators/number-validator.service';

const SERVICES = [
  AdminApiService,
  AuthApiService,
  RestApiBaseService,
  SharedService,
  StaffApiService,
  NumberValidatorService
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})

export class ServicesModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ServicesModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
