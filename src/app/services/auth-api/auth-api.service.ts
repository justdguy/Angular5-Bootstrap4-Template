import { Injectable } from '@angular/core';
import { RestApiBaseService } from './../rest-api-base/rest-api-base.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AuthApiService {

  externalAPIToGetIP = "https://api.ipdata.co";

  constructor(
    private restApiBase: RestApiBaseService,
    public jwtHelper: JwtHelperService,
    private http: HttpClient
  ) { }

  public authenticate(loginCredentials) {
    return this.restApiBase.post('/authenticate', { ...loginCredentials })
  }

  public setSession(authResponse): void {
    if(authResponse["token"]){
      localStorage.setItem('token', authResponse["token"]);
    }
    if(authResponse["user"] && authResponse["user"]["admin"]){
      localStorage.setItem('admin', String(authResponse["user"]["admin"]));
    }
    if(authResponse["user"] && authResponse["user"]["name"]){
      localStorage.setItem('name', authResponse["user"]["name"]);
    }
    if(authResponse["user"] && authResponse["user"]["_id"]){
      localStorage.setItem('userID', authResponse["user"]["_id"]);
    }
  }

  public isAuthenticated(): boolean{
    var t = !this.jwtHelper.isTokenExpired();
    return !this.jwtHelper.isTokenExpired();
  }

  public isAdmin(): boolean {
    var t = Boolean(localStorage.getItem("admin"));
    return (localStorage.getItem("admin")) ? t : null
  }

  public getSession(): Observable<any>{
    return Observable.of({
      isAdmin: this.isAdmin(),
      name: localStorage.getItem("name"),
      userID: localStorage.getItem("userID")
    })
  }

  public clearSession(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('admin');
    localStorage.removeItem('name');
    localStorage.removeItem('userID');
  }

  public getUserIPAddress(){
    return this.http.get(this.externalAPIToGetIP)
  }

}
