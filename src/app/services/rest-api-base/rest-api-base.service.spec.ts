import { TestBed, inject } from '@angular/core/testing';

import { RestApiBaseService } from './rest-api-base.service';

describe('RestApiBaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestApiBaseService]
    });
  });

  it('should be created', inject([RestApiBaseService], (service: RestApiBaseService) => {
    expect(service).toBeTruthy();
  }));
});
