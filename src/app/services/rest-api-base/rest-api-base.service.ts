import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';

@Injectable()
export class RestApiBaseService {

  // apiBase = "http://10.0.5.89:3030/api"; // production
  //  apiBase = "http://66.64.62.114:3030/api";
  apiBase = "http://10.0.5.89:4000/api";

  retry: number = 3;

  constructor(private http: HttpClient) { }

  getHeaders() {
    const token = localStorage.getItem('token');
    return token ? new HttpHeaders().set('x-access-token', token) : null;
    
  }

  get(link: string) {
    return this.http.get(this.apiBase +link, { headers: this.getHeaders() })
    .pipe(
      retry(this.retry), // retry a failed request up to 3 times
      catchError(this.handleError) // then handle the error
    );
  }

  post(link: string, body: any) {
    return this.http.post(this.apiBase + link, body, { headers: this.getHeaders() })
    .pipe(
      catchError(this.handleError) // then handle the error
    );
  }

  update(link: string, body: any) {
    return this.http.put(this.apiBase + link, body, { headers: this.getHeaders() })
    .pipe(
      catchError(this.handleError) // then handle the error
    );
  }

  delete(link: string, body: any) {
    return this.http.delete(this.apiBase + link, { headers: this.getHeaders() })
    .pipe(
      catchError(this.handleError) // then handle the error
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something went wrong; please try again later.');
  };

}
