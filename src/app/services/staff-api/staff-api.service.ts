import { Injectable } from '@angular/core';
import { RestApiBaseService } from './../rest-api-base/rest-api-base.service';

@Injectable()
export class StaffApiService {

  constructor(
    private restApiBase: RestApiBaseService
  ) { }

  addTimesheet(timesheetObject){
    if(timesheetObject){
      timesheetObject["userID"] = localStorage.getItem("userID");
    }
    return this.restApiBase.post("/addTimesheet", {...timesheetObject})
  }

  getUserTodayTimesheets(){
    return this.restApiBase.get("/todayTimesheets?userID=" + localStorage.getItem("userID") )
  }

  getAllUserTimesheets(){
    return this.restApiBase.get("/userTimesheets?userID=" + localStorage.getItem("userID") )
  }

  getAllUserTimesheetsByMonth(month) {
    return this.restApiBase.post("/monthlyTimesheets ", {userID: localStorage.getItem("userID"), date: month})
  }
  // getAllProjects(){
  //   return this.restApiBase.get("/projects" + localStorage.getItem("userID"))
  // }
  
}
