import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { IsAdminGuard } from './is-admin/is-admin.guard';
import { IsAuthenticatedGuard } from './is-authenticated/is-authenticated.guard';

export const guards = [
  IsAdminGuard,
  IsAuthenticatedGuard
]

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...guards,
  ],
})
export class GuardsModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: GuardsModule,
      providers: [
        ...guards,
      ],
    };
  }
}
