import { Injectable } from '@angular/core';
import { CanActivateChild, 
  ActivatedRouteSnapshot, 
  RouterStateSnapshot, 
  Router, 
  ActivatedRoute 
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthApiService } from './../../services/auth-api/auth-api.service';

@Injectable()
export class IsAdminGuard implements CanActivateChild{
  constructor(
    private authService: AuthApiService,
    private router: Router,
  ) {}; 

  canActivateChild() {
    if (this.authService.isAdmin()) { 
      return true;
    } else {
      // this.router.navigateByUrl('/login');
      return false;
    }
  }
}
