import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthApiService } from './../../services/auth-api/auth-api.service';

@Injectable()
export class IsAuthenticatedGuard implements CanActivate {
  constructor(
    private authService: AuthApiService,
    private router: Router,
  ) {}; 

  canActivate() {
    if (this.authService.isAuthenticated()) { 
      return true;
    } else {
      
      this.router.navigateByUrl('/auth');
      return false;
    }
  }
}
